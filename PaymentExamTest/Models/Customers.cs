﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentExamTest.Models
{
    public class Customers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string customerID { get; set; }
        public string name { get; set; }
        [EmailAddress]
        public string email { get; set; }
        [Phone]
        public string mobile { get; set; }
        //Single customer has many transaction 
        public ICollection<Transactions> transactions { get; set; }
    }

}