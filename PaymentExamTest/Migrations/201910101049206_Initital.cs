namespace PaymentExamTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        customerID = c.String(),
                        name = c.String(),
                        email = c.String(),
                        mobile = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false,defaultValueSql: "GETDATE()"),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        currency = c.String(),
                        status = c.String(),
                        customerID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Customers", t => t.customerID, cascadeDelete: true)
                .Index(t => t.customerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "customerID", "dbo.Customers");
            DropIndex("dbo.Transactions", new[] { "customerID" });
            DropTable("dbo.Transactions");
            DropTable("dbo.Customers");
        }
    }
}
