﻿using System.Net.Mail;

namespace PaymentExamTest.Tools
{
    public static class Extension
    {
        public static bool IsEmail(this string text)
        {
            try
            {
                new MailAddress(text);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}