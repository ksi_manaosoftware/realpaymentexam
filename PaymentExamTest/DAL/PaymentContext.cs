﻿using PaymentExamTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PaymentExamTest.DAL
{
    public class PaymentContext : DbContext
    {
        public DbSet<Customers> CustomersContext { get; set; }
        public DbSet<Transactions> TransactionsContext { get; set; }

        public PaymentContext() : base ("PaymentDB") // select connectionString database
        {

        }

    }
}