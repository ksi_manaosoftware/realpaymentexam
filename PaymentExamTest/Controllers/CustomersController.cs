﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using PaymentExamTest.DAL;
using PaymentExamTest.Models;
using PaymentExamTest.Tools;

namespace PaymentExamTest.Controllers
{
    public class CustomersController : ApiController
    {
        private PaymentContext paymentContext = new PaymentContext();

        //public CustomersController(PaymentContext paymentContext)
        //{
        //    this.paymentContext = paymentContext;
        //}

        //GET /api/Customers
        public IHttpActionResult GetCustomerTransactionHistory(string email = null, string customerID = null)
        {
            if (HttpContext.Current.Request.QueryString.AllKeys.Count() == 0)
            {
                return BadRequest("No inquiry criteria");
            }
            var allParams = this.GetType().GetMethod(nameof(this.GetCustomerTransactionHistory)).GetParameters().Select(x => x.Name);
            var allQueryKey = HttpContext.Current.Request.QueryString.ToString().Split('&').Select(x => HttpUtility.UrlDecode(x.Split('=').First()));
            bool isInvalid = !allQueryKey.Aggregate(true, (y, z) => y && allParams.Contains(z)); // check if a parameter exist and no strange parameter 
            if (isInvalid)
                return BadRequest(); // OtherCase

            long temp; // for cast int

            IQueryable<Customers> query = this.paymentContext.CustomersContext.Include(x => x.transactions);
            if (allQueryKey.Contains(nameof(email)))
            {
                if (!email.IsEmail())
                    return BadRequest("Invalid Email");
                query = query.Where(x => x.email == email);
            }
            if (allQueryKey.Contains(nameof(customerID)))
                if (long.TryParse(customerID, out temp))
                    query = query.Where(x => x.customerID == customerID);
                else
                    return BadRequest("Invalid Customer ID");
            var result = query.FirstOrDefault();
            if (result != null)
                result.transactions = result.transactions.OrderByDescending(x => x.date).Take(5).ToList();
            else
                return Ok("Not found");
            return Json(result);
        }

    }
}